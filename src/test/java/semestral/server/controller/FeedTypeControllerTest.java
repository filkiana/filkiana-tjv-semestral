package semestral.server.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.dto.FeedTypeCreateDTO;
import semestral.server.dto.FeedTypeDTO;
import semestral.server.service.FeedStorageService;
import semestral.server.service.FeedTypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class FeedTypeControllerTest {
    @Autowired
    private FeedTypeController feedTypeController;

    @MockBean
    private FeedTypeService feedTypeService;
    @Test
    void all() {

        List<FeedTypeDTO> feedTypeDTOList = new ArrayList<>();
        feedTypeDTOList.add(new FeedTypeDTO(10, "Apples",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        feedTypeDTOList.add(new FeedTypeDTO(11, "Bananas",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        BDDMockito.given(feedTypeService.findAllAsDTO()).willReturn(feedTypeDTOList);

        Assertions.assertEquals(feedTypeDTOList, feedTypeController.all());
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).findAllAsDTO();
    }

    @Test
    void byId() {
        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(10, "Apples",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(feedTypeService.findByIdAsDTO(10)).willReturn(Optional.of(feedTypeDTO));

        Assertions.assertEquals(feedTypeDTO, feedTypeController.byId(feedTypeDTO.getId()));
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).findByIdAsDTO(feedTypeDTO.getId());
    }

    @Test
    void byTypeName() {
        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(10, "Apples",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(feedTypeService.findByTypeNameAsDTO(feedTypeDTO.getTypeName())).willReturn(Optional.of(feedTypeDTO));

        Assertions.assertEquals(feedTypeDTO, feedTypeController.byTypeName(feedTypeDTO.getTypeName()));
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).findByTypeNameAsDTO(feedTypeDTO.getTypeName());
    }

    @Test
    void create() throws IllegalArgumentException {
        Integer id = 10;
        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, "Apples",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        FeedTypeCreateDTO feedTypeCreateDTO = new FeedTypeCreateDTO( "Bananas",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(feedTypeService.create(feedTypeCreateDTO)).willReturn(feedTypeDTO);

        Assertions.assertEquals(feedTypeDTO, feedTypeController.create(feedTypeCreateDTO));
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).create(feedTypeCreateDTO);
    }

    @Test
    void update() throws IllegalArgumentException {
        Integer id = 10;
        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, "Apples",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        FeedTypeCreateDTO feedTypeCreateDTO = new FeedTypeCreateDTO( "Bananas",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(feedTypeService.update(id, feedTypeCreateDTO)).willReturn(feedTypeDTO);

        Assertions.assertEquals(feedTypeDTO, feedTypeController.update(id, feedTypeCreateDTO));
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).update(id, feedTypeCreateDTO);
    }
}