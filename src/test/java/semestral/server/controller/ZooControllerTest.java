package semestral.server.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import semestral.server.dto.ZooCreateDTO;
import semestral.server.dto.ZooDTO;
import semestral.server.service.ZooService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class ZooControllerTest {
    @Autowired
    private ZooController zooController;

    @MockBean
    private ZooService zooService;

    @Test
    void all() {
        List<ZooDTO> zooDTOList = new ArrayList<>();
        zooDTOList.add(new ZooDTO(10, 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        zooDTOList.add(new ZooDTO(11, 11,
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        BDDMockito.given(zooService.findAllAsDTO()).willReturn(zooDTOList);

        Assertions.assertEquals(zooDTOList, zooController.all());
        Mockito.verify(zooService, Mockito.atLeastOnce()).findAllAsDTO();
    }

    @Test
    void byId() {
        ZooDTO zooDTO = new ZooDTO(10, 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));
        BDDMockito.given(zooService.findByIdAsDTO(10)).willReturn(Optional.of(zooDTO));

        Assertions.assertEquals(zooDTO, zooController.byId(zooDTO.getId()));
        Mockito.verify(zooService, Mockito.atLeastOnce()).findByIdAsDTO(zooDTO.getId());
    }

    @Test
    void create() throws IllegalArgumentException {
        ZooDTO zooDTO = new ZooDTO(10, 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        ZooCreateDTO zooCreateDTO = new ZooCreateDTO( 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));
        BDDMockito.given(zooService.create(zooCreateDTO)).willReturn(zooDTO);

        Assertions.assertEquals(zooDTO, zooController.create(zooCreateDTO));
        Mockito.verify(zooService, Mockito.atLeastOnce()).create(zooCreateDTO);
    }

    @Test
    void update() throws IllegalArgumentException {

        Integer id = 10;
        ZooDTO zooDTO = new ZooDTO(10, 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        ZooCreateDTO zooCreateDTO = new ZooCreateDTO( 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));


        BDDMockito.given(zooService.update(id, zooCreateDTO)).willReturn(zooDTO);

        Assertions.assertEquals(zooDTO, zooController.update(id, zooCreateDTO));
        Mockito.verify(zooService, Mockito.atLeastOnce()).update(id, zooCreateDTO);
    }

    @Test
    void allOpened() {
        List<ZooDTO> zooDTOList = new ArrayList<>();
        zooDTOList.add(new ZooDTO(10, 10,
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        zooDTOList.add(new ZooDTO(11, 11,
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        BDDMockito.given(zooService.findAllOpenedAsDTO()).willReturn(zooDTOList);

        Assertions.assertEquals(zooDTOList, zooController.allOpened());
        Mockito.verify(zooService, Mockito.atLeastOnce()).findAllOpenedAsDTO();
    }
}