package semestral.server.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.AnimalTypeCreateDTO;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.dto.ZooAddressCreateDTO;
import semestral.server.dto.ZooAddressDTO;
import semestral.server.service.AnimalTypeService;
import semestral.server.service.ZooAddressService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class AnimalTypeControllerTest {

    @Autowired
    private AnimalTypeController animalTypeController;

    @MockBean
    private AnimalTypeService animalTypeService;
    @Test
    void all() {
        List<AnimalTypeDTO> animalTypeDTOList = new ArrayList<>();
        animalTypeDTOList.add(new AnimalTypeDTO(10, "Cat",
                Arrays.stream("1,2".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        animalTypeDTOList.add(new AnimalTypeDTO(11, "Dog",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        BDDMockito.given(animalTypeService.findAllAsDTO()).willReturn(animalTypeDTOList);

        Assertions.assertEquals(animalTypeDTOList, animalTypeController.all());
        Mockito.verify(animalTypeService, Mockito.atLeastOnce()).findAllAsDTO();
    }

    @Test
    void byId() {
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(10, "Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(animalTypeService.findByIdAsDTO(10)).willReturn(Optional.of(animalTypeDTO));

        Assertions.assertEquals(animalTypeDTO, animalTypeController.byId(animalTypeDTO.getId()));
        Mockito.verify(animalTypeService, Mockito.atLeastOnce()).findByIdAsDTO(animalTypeDTO.getId());
    }

    @Test
    void byTypeName() {
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(10, "Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(animalTypeService.findByTypeNameAsDTO(animalTypeDTO.getTypeName())).willReturn(Optional.of(animalTypeDTO));

        Assertions.assertEquals(animalTypeDTO, animalTypeController.byTypeName(animalTypeDTO.getTypeName()));
        Mockito.verify(animalTypeService, Mockito.atLeastOnce()).findByTypeNameAsDTO(animalTypeDTO.getTypeName());
    }

    @Test
    void create() throws IllegalArgumentException {
        AnimalTypeCreateDTO animalTypeCreateDTO = new AnimalTypeCreateDTO("Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(10, "Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(animalTypeService.create(animalTypeCreateDTO)).willReturn(animalTypeDTO);

        Assertions.assertEquals(animalTypeDTO, animalTypeController.create(animalTypeCreateDTO));
        Mockito.verify(animalTypeService, Mockito.atLeastOnce()).create(animalTypeCreateDTO);
    }

    @Test
    void update() throws IllegalArgumentException {
        Integer id = 10;
        AnimalTypeCreateDTO animalTypeCreateDTO = new AnimalTypeCreateDTO("Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(id, "Cat",
                Arrays.stream("1,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                Arrays.stream("2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList()));

        BDDMockito.given(animalTypeService.update(id, animalTypeCreateDTO)).willReturn(animalTypeDTO);

        Assertions.assertEquals(animalTypeDTO, animalTypeController.update(id, animalTypeCreateDTO));
        Mockito.verify(animalTypeService, Mockito.atLeastOnce()).update(id, animalTypeCreateDTO);
    }
}