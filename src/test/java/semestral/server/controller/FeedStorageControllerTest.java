package semestral.server.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.dto.FeedStorageCreateDTO;
import semestral.server.dto.FeedStorageDTO;
import semestral.server.entity.FeedStorage;
import semestral.server.service.AnimalTypeService;
import semestral.server.service.FeedStorageService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class FeedStorageControllerTest {
    @Autowired
    private FeedStorageController feedStorageController;

    @MockBean
    private FeedStorageService feedStorageService;
    @Test
    void all() {
        List<FeedStorageDTO> feedStorageDTOList = new ArrayList<>();
        feedStorageDTOList.add(new FeedStorageDTO(10, 2, 1, 100));
        feedStorageDTOList.add(new FeedStorageDTO(11, 3, 2, 100));
        BDDMockito.given(feedStorageService.findAllAsDTO()).willReturn(feedStorageDTOList);

        Assertions.assertEquals(feedStorageDTOList, feedStorageController.all());
        Mockito.verify(feedStorageService, Mockito.atLeastOnce()).findAllAsDTO();
    }

    @Test
    void byId() {
        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(10, 2, 1, 100);
        BDDMockito.given(feedStorageService.findByIdAsDTO(10)).willReturn(Optional.of(feedStorageDTO));

        Assertions.assertEquals(feedStorageDTO, feedStorageController.byId(feedStorageDTO.getId()));
        Mockito.verify(feedStorageService, Mockito.atLeastOnce()).findByIdAsDTO(feedStorageDTO.getId());
    }

    @Test
    void create() throws IllegalArgumentException {
        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(10, 2, 1, 100);
        FeedStorageCreateDTO feedStorageCreateDTO = new FeedStorageCreateDTO( 2, 1, 100);

        BDDMockito.given(feedStorageService.create(feedStorageCreateDTO)).willReturn(feedStorageDTO);

        Assertions.assertEquals(feedStorageDTO, feedStorageController.create(feedStorageCreateDTO));
        Mockito.verify(feedStorageService, Mockito.atLeastOnce()).create(feedStorageCreateDTO);
    }

    @Test
    void update() throws IllegalArgumentException {
        Integer id = 10;
        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(id, 2, 1, 100);
        FeedStorageCreateDTO feedStorageCreateDTO = new FeedStorageCreateDTO( 2, 1, 100);

        BDDMockito.given(feedStorageService.update(id, feedStorageCreateDTO)).willReturn(feedStorageDTO);

        Assertions.assertEquals(feedStorageDTO, feedStorageController.update(id, feedStorageCreateDTO));
        Mockito.verify(feedStorageService, Mockito.atLeastOnce()).update(id, feedStorageCreateDTO);
    }
}