package semestral.server.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import semestral.server.dto.ZooAddressCreateDTO;
import semestral.server.dto.ZooAddressDTO;
import semestral.server.entity.ZooAddress;
import semestral.server.service.ZooAddressService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ZooAddressControllerTest {

    @Autowired
    private ZooAddressController zooAddressController;

    @MockBean
    private ZooAddressService zooAddressService;
    @Test
    void all() {
        List<ZooAddressDTO> zooAddressDTOList = new ArrayList<>();
        zooAddressDTOList.add(new ZooAddressDTO(10, "Prague", "Karlovo Namesti", 14));
        zooAddressDTOList.add(new ZooAddressDTO(11, "Brno", "Karlovo Namesti", 15));

        BDDMockito.given(zooAddressService.findAllAsDTO()).willReturn(zooAddressDTOList);

        Assertions.assertEquals(zooAddressDTOList, zooAddressController.all());
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findAllAsDTO();
    }

    @Test
    void byId() {
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO(10, "Prague", "Karlovo Namesti", 14);

        BDDMockito.given(zooAddressService.findByIdAsDTO(10)).willReturn(Optional.of(zooAddressDTO));

        Assertions.assertEquals(zooAddressDTO, zooAddressController.byId(zooAddressDTO.getId()));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findByIdAsDTO(zooAddressDTO.getId());
    }

    @Test
    void byCity() {
        List<ZooAddressDTO> zooAddressDTOList = new ArrayList<>();
        String city = "Prague";
        zooAddressDTOList.add(new ZooAddressDTO(10, city, "Karlovo Namesti", 14));
        zooAddressDTOList.add(new ZooAddressDTO(11, city, "Karlovo Namesti", 15));

        BDDMockito.given(zooAddressService.findAllByCityAsDTO(city)).willReturn(zooAddressDTOList);

        Assertions.assertEquals(zooAddressDTOList, zooAddressController.byCity(city));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findAllByCityAsDTO(city);
    }

    @Test
    void byStreet() {
        List<ZooAddressDTO> zooAddressDTOList = new ArrayList<>();
        String street = "Karlovo Namesti";
        zooAddressDTOList.add(new ZooAddressDTO(10, "Prague", street, 14));
        zooAddressDTOList.add(new ZooAddressDTO(11, "Brno", street, 15));

        BDDMockito.given(zooAddressService.findAllByStreetAsDTO(street)).willReturn(zooAddressDTOList);

        Assertions.assertEquals(zooAddressDTOList, zooAddressController.byStreet(street));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findAllByStreetAsDTO(street);
    }

    @Test
    void byHouseNumber() {
        List<ZooAddressDTO> zooAddressDTOList = new ArrayList<>();
        Integer houseNumber = 20;
        zooAddressDTOList.add(new ZooAddressDTO(10, "Prague", "Karlovo Namesti", houseNumber));
        zooAddressDTOList.add(new ZooAddressDTO(11, "Brno", "Namesti Republiky", houseNumber));

        BDDMockito.given(zooAddressService.findAllByHouseNumberAsDTO(houseNumber)).willReturn(zooAddressDTOList);

        Assertions.assertEquals(zooAddressDTOList, zooAddressController.byHouseNumber(houseNumber));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findAllByHouseNumberAsDTO(houseNumber);
    }

    @Test
    void create() throws IllegalArgumentException {
        ZooAddressCreateDTO zooAddressCreateDTO = new ZooAddressCreateDTO( "Prague", "Karlovo Namesti", 14);
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO(10 , "Prague", "Karlovo Namesti", 14);

        BDDMockito.given(zooAddressService.create(zooAddressCreateDTO)).willReturn(zooAddressDTO);

        Assertions.assertEquals(zooAddressDTO, zooAddressController.create(zooAddressCreateDTO));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).create(zooAddressCreateDTO);
    }

    @Test
    void update() throws IllegalArgumentException {
        Integer id = 10;
        ZooAddressCreateDTO zooAddressCreateDTO = new ZooAddressCreateDTO( "Prague", "Karlovo Namesti", 14);
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO(id , "Prague", "Karlovo Namesti", 14);

        BDDMockito.given(zooAddressService.update(id, zooAddressCreateDTO)).willReturn(zooAddressDTO);

        Assertions.assertEquals(zooAddressDTO, zooAddressController.update(id, zooAddressCreateDTO));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).update(id, zooAddressCreateDTO);
    }

}