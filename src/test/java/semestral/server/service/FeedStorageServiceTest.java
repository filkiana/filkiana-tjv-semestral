package semestral.server.service;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.FeedStorageCreateDTO;
import semestral.server.dto.FeedStorageDTO;
import semestral.server.entity.FeedStorage;
import semestral.server.entity.FeedType;
import semestral.server.entity.Zoo;
import semestral.server.repository.FeedStorageRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class FeedStorageServiceTest {

    @Autowired
    private FeedStorageService feedStorageService;
    @MockBean
    private FeedStorageRepository feedStorageRepository;
    @MockBean
    private FeedTypeService feedTypeService;
    @MockBean
    private ZooService zooService;

    @Test
    void findByIdAsDTO() {
        Integer id = 0;
        FeedType feedType = new FeedType();
        Zoo zoo = new Zoo();
        FeedStorage feedStorage= new FeedStorage( feedType, 100, zoo);

        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(id, feedType.getFeedTypeId(), zoo.getZooId(), 100);

        BDDMockito.given(feedStorageRepository.findByFeedStorageId(id)).willReturn(Optional.of(feedStorage));
        Assertions.assertEquals(feedStorageDTO.getId(), feedStorageService.findByIdAsDTO(id).get().getId());
        Assertions.assertEquals(feedStorageDTO.getFeedTypeId(), feedStorageService.findByIdAsDTO(id).get().getFeedTypeId());
        Assertions.assertEquals(feedStorageDTO.getZooId(), feedStorageService.findByIdAsDTO(id).get().getZooId());
        Assertions.assertEquals(feedStorageDTO.getCount(), feedStorageService.findByIdAsDTO(id).get().getCount());

        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).findByFeedStorageId(id);
    }

    @Test
    void findByIds() {
        List<Integer>ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        List<FeedStorage> feedStorageList = new ArrayList<>();
        FeedType feedType = new FeedType();
        Zoo zoo = new Zoo();
        feedStorageList.add(new FeedStorage(   feedType, 100, zoo));
        feedStorageList.add(new FeedStorage(  feedType, 102, zoo));


        BDDMockito.given(feedStorageRepository.findAllById(ids)).willReturn(feedStorageList);

        Assertions.assertEquals(feedStorageList, feedStorageService.findByIds(ids));
        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).findAllById(ids);
    }

    @Test
    void findAll() {
        List<FeedStorage> feedStorageList = new ArrayList<>();
        FeedType feedType = new FeedType();
        Zoo zoo = new Zoo();
        feedStorageList.add(new FeedStorage(   feedType, 100, zoo));
        feedStorageList.add(new FeedStorage(  feedType, 102, zoo));
        BDDMockito.given(feedStorageRepository.findAll()).willReturn(feedStorageList);

        Assertions.assertEquals(feedStorageList, feedStorageService.findAll());
        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllAsDTO() {
        List<FeedStorage> feedStorageList = new ArrayList<>();
        FeedType feedType = new FeedType();
        Zoo zoo = new Zoo();
        feedStorageList.add(new FeedStorage(   feedType, 100, zoo));
        feedStorageList.add(new FeedStorage(  feedType, 102, zoo));
        BDDMockito.given(feedStorageRepository.findAll()).willReturn(feedStorageList);
        Assertions.assertEquals(feedStorageList.stream().map(FeedStorage::getFeedStorageId).collect(Collectors.toList()),
                feedStorageService.findAllAsDTO().stream().map(FeedStorageDTO::getId).collect(Collectors.toList()));
        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void create() {
        Integer id = 0;
        FeedType feedType = new FeedType("beans", new ArrayList<>(), new ArrayList<>());
        Zoo zoo = new Zoo();
        FeedStorage feedStorage= new FeedStorage( feedType, 0, zoo);
        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(id, feedType.getFeedTypeId(), zoo.getZooId(), 0);
        FeedStorageCreateDTO feedStorageCreateDTO = new FeedStorageCreateDTO( feedType.getFeedTypeId(), zoo.getZooId(), 0);

        BDDMockito.given(feedStorageRepository.save(Mockito.any(FeedStorage.class))).willReturn(feedStorage);
        BDDMockito.given(feedTypeService.findById(Mockito.any(Integer.class))).willReturn(Optional.of(feedType));
        BDDMockito.given(zooService.findById(Mockito.any(Integer.class))).willReturn(Optional.of(zoo));

        FeedStorageDTO result = feedStorageService.create(feedStorageCreateDTO);
        Assertions.assertEquals(feedStorageDTO.getId(), result.getId());
        Assertions.assertEquals(feedStorageDTO.getCount(), result.getCount());
        Assertions.assertEquals(feedStorageDTO.getFeedTypeId(), result.getFeedTypeId());
        Assertions.assertEquals(feedStorageDTO.getZooId(), result.getZooId());

        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).save(Mockito.any(FeedStorage.class));
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).findById(Mockito.any(Integer.class));
        Mockito.verify(zooService, Mockito.atLeastOnce()).findById(Mockito.any(Integer.class));
    }

    @Test
    void update() {
        Integer id = 0;
        FeedType feedType = new FeedType();
        Zoo zoo = new Zoo();
        FeedStorage feedStorage= new FeedStorage( feedType, 0, zoo);
        FeedStorageDTO feedStorageDTO = new FeedStorageDTO(id, feedType.getFeedTypeId(), zoo.getZooId(), 0);
        FeedStorageCreateDTO feedStorageCreateDTO = new FeedStorageCreateDTO( feedType.getFeedTypeId(), zoo.getZooId(), 0);

        BDDMockito.given(feedStorageRepository.findByFeedStorageId(id)).willReturn(Optional.of(feedStorage));
        BDDMockito.given(feedTypeService.findById(Mockito.any(Integer.class))).willReturn(Optional.of(feedType));
        BDDMockito.given(zooService.findById(Mockito.any(Integer.class))).willReturn(Optional.of(zoo));

        FeedStorageDTO result = feedStorageService.update(id, feedStorageCreateDTO);
        Assertions.assertEquals(feedStorageDTO.getId(), result.getId());
        Assertions.assertEquals(feedStorageDTO.getCount(), result.getCount());
        Assertions.assertEquals(feedStorageDTO.getFeedTypeId(), result.getFeedTypeId());
        Assertions.assertEquals(feedStorageDTO.getZooId(), result.getZooId());

        Mockito.verify(feedStorageRepository, Mockito.atLeastOnce()).findByFeedStorageId(id);
        Mockito.verify(feedTypeService, Mockito.atLeastOnce()).findById(Mockito.any(Integer.class));
        Mockito.verify(zooService, Mockito.atLeastOnce()).findById(Mockito.any(Integer.class));
    }
}