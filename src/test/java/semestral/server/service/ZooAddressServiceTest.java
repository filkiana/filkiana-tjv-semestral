package semestral.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.ZooAddressCreateDTO;
import semestral.server.dto.ZooAddressDTO;
import semestral.server.entity.ZooAddress;
import semestral.server.repository.ZooAddressRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class ZooAddressServiceTest {

    @Autowired
    private ZooAddressService zooAddressService;
    @MockBean
    private ZooAddressRepository zooAddressRepository;

    @Test
    void findById() {
        Integer id = 0;
        ZooAddress zooAddress = new ZooAddress( "Prague", "Karlovo Namesti", 14);
        BDDMockito.given(zooAddressRepository.findByZooAddressId(id)).willReturn(Optional.of(zooAddress));
        Assertions.assertEquals(zooAddress, zooAddressService.findById(id).get());
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findByZooAddressId(id);
    }

    @Test
    void findByIdAsDTO() {
        Integer id = 0;
        ZooAddress zooAddress = new ZooAddress( "Prague", "Karlovo Namesti", 14);
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO( id,"Prague", "Karlovo Namesti", 14);
        BDDMockito.given(zooAddressRepository.findByZooAddressId(id)).willReturn(Optional.of(zooAddress));
        Assertions.assertEquals(zooAddressDTO.getId(), zooAddressService.findByIdAsDTO(id).get().getId());
        Assertions.assertEquals(zooAddressDTO.getCity(), zooAddressService.findByIdAsDTO(id).get().getCity());
        Assertions.assertEquals(zooAddressDTO.getStreet(), zooAddressService.findByIdAsDTO(id).get().getStreet());
        Assertions.assertEquals(zooAddressDTO.getHouseNumber(), zooAddressService.findByIdAsDTO(id).get().getHouseNumber());
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findByZooAddressId(id);
    }

    @Test
    void findAll() {
        List<ZooAddress> zooAddressList = new ArrayList<>();
        zooAddressList.add(new ZooAddress( "Prague", "Karlovo Namesti", 14));
        zooAddressList.add(new ZooAddress( "Brno", "Karlovo Namesti", 15));
        BDDMockito.given(zooAddressRepository.findAll()).willReturn(zooAddressList);
        Assertions.assertEquals(zooAddressList, zooAddressService.findAll());
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllByCityAsDTO() {
        String city = "Prague";
        List<ZooAddress> zooAddressList = new ArrayList<>();
        zooAddressList.add(new ZooAddress( city, "Karlovo Namesti", 14));
        zooAddressList.add(new ZooAddress( city, "Karlovo Namesti", 15));
        BDDMockito.given(zooAddressRepository.findAddressByCity(city)).willReturn(zooAddressList);
        Assertions.assertEquals(zooAddressList.stream().map(ZooAddress::getZooAddressId).collect(Collectors.toList()),
                zooAddressService.findAllByCityAsDTO(city).stream().map(ZooAddressDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findAddressByCity(city);

    }

    @Test
    void findAllByStreetAsDTO() {
        String street = "Karlovo Namesti";
        List<ZooAddress> zooAddressList = new ArrayList<>();
        zooAddressList.add(new ZooAddress( "Prague", street, 14));
        zooAddressList.add(new ZooAddress("Brno", street, 15));
        BDDMockito.given(zooAddressRepository.findAddressByStreet(street)).willReturn(zooAddressList);
        Assertions.assertEquals(zooAddressList.stream().map(ZooAddress::getZooAddressId).collect(Collectors.toList()),
                zooAddressService.findAllByStreetAsDTO(street).stream().map(ZooAddressDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findAddressByStreet(street);

    }

    @Test
    void findAllByHouseNumberAsDTO() {
        Integer houseNumber = 14;
        List<ZooAddress> zooAddressList = new ArrayList<>();
        zooAddressList.add(new ZooAddress( "Prague", "Karlovo Namesti", houseNumber));
        zooAddressList.add(new ZooAddress( "Brno", "Karlovo Namesti", houseNumber));
        BDDMockito.given(zooAddressRepository.findAddressByHouseNumber(houseNumber)).willReturn(zooAddressList);
        Assertions.assertEquals(zooAddressList.stream().map(ZooAddress::getZooAddressId).collect(Collectors.toList()),
                zooAddressService.findAllByHouseNumberAsDTO(houseNumber).stream().map(ZooAddressDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findAddressByHouseNumber(houseNumber);

    }

    @Test
    void findAllAsDTO() {
        List<ZooAddress> zooAddressList = new ArrayList<>();
        zooAddressList.add(new ZooAddress( "Prague", "Karlovo Namesti", 14));
        zooAddressList.add(new ZooAddress( "Brno", "Karlovo Namesti", 15));
        BDDMockito.given(zooAddressRepository.findAll()).willReturn(zooAddressList);
        Assertions.assertEquals(zooAddressList.stream().map(ZooAddress::getZooAddressId).collect(Collectors.toList()),
                zooAddressService.findAllAsDTO().stream().map(ZooAddressDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findAll();
    }
    @Test
    void create() throws IllegalArgumentException {
        Integer id = 0;
        ZooAddress zooAddress = new ZooAddress( "Prague", "Karlovo Namesti", 14);
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO( id,"Prague", "Karlovo Namesti", 14);
        ZooAddressCreateDTO zooAddressCreateDTO = new ZooAddressCreateDTO("Prague", "Karlovo Namesti", 14);

        BDDMockito.given(zooAddressRepository.save(Mockito.any(ZooAddress.class))).willReturn(zooAddress);

        ZooAddressDTO result = zooAddressService.create(zooAddressCreateDTO);
        Assertions.assertEquals(zooAddressDTO.getId(), result.getId());
        Assertions.assertEquals(zooAddressDTO.getCity(), result.getCity());
        Assertions.assertEquals(zooAddressDTO.getStreet(), result.getStreet());
        Assertions.assertEquals(zooAddressDTO.getHouseNumber(), result.getHouseNumber());

        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).save(Mockito.any(ZooAddress.class));

    }

    @Test
    void update() {
        Integer id = 0;
        ZooAddress zooAddress =   new ZooAddress( "Prague", "Karlovo Namesti", 14);
        ZooAddressDTO zooAddressDTO = new ZooAddressDTO( id,"Prague", "Karlovo Namesti", 14);
        ZooAddressCreateDTO zooAddressCreateDTO = new ZooAddressCreateDTO("Prague", "Karlovo Namesti", 14);

        BDDMockito.given(zooAddressRepository.findByZooAddressId(id)).willReturn(Optional.of(zooAddress));

        ZooAddressDTO result = zooAddressService.update(id, zooAddressCreateDTO);

        Assertions.assertEquals(zooAddressDTO.getId(), result.getId());
        Assertions.assertEquals(zooAddressDTO.getCity(), result.getCity());
        Assertions.assertEquals(zooAddressDTO.getStreet(), result.getStreet());
        Assertions.assertEquals(zooAddressDTO.getHouseNumber(), result.getHouseNumber());

        Mockito.verify(zooAddressRepository, Mockito.atLeastOnce()).findByZooAddressId(id);

    }

}