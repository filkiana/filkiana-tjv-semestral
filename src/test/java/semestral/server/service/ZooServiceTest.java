package semestral.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.ZooCreateDTO;
import semestral.server.dto.ZooDTO;
import semestral.server.entity.*;
import semestral.server.repository.ZooRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class ZooServiceTest {

    @Autowired
    private ZooService zooService;
    @MockBean
    private ZooRepository zooRepository;
    @MockBean
    private ZooAddressService zooAddressService;
    @Test
    void findById() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress= new ZooAddress();
        Zoo zoo= new Zoo( zooAddress, feedStorages, animalTypes);

        BDDMockito.given(zooRepository.findByZooId(id)).willReturn(Optional.of(zoo));
        Assertions.assertEquals(zoo, zooService.findById(id).get());

        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findByZooId(id);
    }

    @Test
    void findAllOpenedAsDTO() {
        List<Zoo> zooList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress = new ZooAddress();
        zooList.add(new Zoo( null, feedStorages, animalTypes));
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        List<Integer> res_ids = new ArrayList<>();
        res_ids.add(0);
        BDDMockito.given(zooRepository.findAll()).willReturn(zooList);

        Assertions.assertEquals(res_ids,
                zooService.findAllOpenedAsDTO().stream().map(ZooDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIdAsDTO() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress= new ZooAddress();
        Zoo zoo= new Zoo( zooAddress, feedStorages, animalTypes);
        ZooDTO zooDTO = new ZooDTO(id, 0, new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(zooRepository.findByZooId(id)).willReturn(Optional.of(zoo));
        Assertions.assertEquals(zooDTO.getId(), zooService.findByIdAsDTO(id).get().getId());
        Assertions.assertEquals(zooDTO.getAddress(), zooService.findByIdAsDTO(id).get().getAddress());
        Assertions.assertEquals(zooDTO.getAnimalTypeIds(), zooService.findByIdAsDTO(id).get().getAnimalTypeIds());
        Assertions.assertEquals(zooDTO.getFeedStorageIds(), zooService.findByIdAsDTO(id).get().getFeedStorageIds());

        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findByZooId(id);
    }

    @Test
    void findByIds() {
        List<Integer>ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        List<Zoo> zooList = new ArrayList<>();
        ZooAddress zooAddress = new ZooAddress();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();

        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        BDDMockito.given(zooRepository.findAllById(ids)).willReturn(zooList);

        Assertions.assertEquals(zooList, zooService.findByIds(ids));
        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findAllById(ids);
    }

    @Test
    void findAll() {
        List<Zoo> zooList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress = new ZooAddress();
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        BDDMockito.given(zooRepository.findAll()).willReturn(zooList);

        Assertions.assertEquals(zooList, zooService.findAll());
        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllAsDTO() {
        List<Zoo> zooList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress = new ZooAddress();
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));
        zooList.add(new Zoo( zooAddress, feedStorages, animalTypes));

        BDDMockito.given(zooRepository.findAll()).willReturn(zooList);

        Assertions.assertEquals(zooList.stream().map(Zoo::getZooId).collect(Collectors.toList()),
                zooService.findAllAsDTO().stream().map(ZooDTO::getId).collect(Collectors.toList()));
        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void create() {

        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress= new ZooAddress();
        Zoo zoo= new Zoo( zooAddress, feedStorages, animalTypes);
        ZooDTO zooDTO = new ZooDTO(id, 0, new ArrayList<Integer>(), new ArrayList<Integer>());
        ZooCreateDTO zooCreateDTO = new ZooCreateDTO(0,  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(zooRepository.save(any(Zoo.class))).willReturn(zoo);
        BDDMockito.given(zooAddressService.findById(any(Integer.class))).willReturn(Optional.of(zooAddress));

        ZooDTO result = zooService.create(zooCreateDTO);
        Assertions.assertEquals(zooDTO.getId(), result.getId());
        Assertions.assertEquals(zooDTO.getFeedStorageIds(), result.getFeedStorageIds());
        Assertions.assertEquals(zooDTO.getAnimalTypeIds(), result.getAnimalTypeIds());
        Assertions.assertEquals(zooDTO.getAddress(), result.getAddress());

        Mockito.verify(zooRepository, Mockito.atLeastOnce()).save(any(Zoo.class));
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findById(any(Integer.class));
    }

    @Test
    void update() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        ZooAddress zooAddress= new ZooAddress();
        Zoo zoo= new Zoo( zooAddress, feedStorages, animalTypes);
        ZooDTO zooDTO = new ZooDTO(id, 0, new ArrayList<Integer>(), new ArrayList<Integer>());
        ZooCreateDTO zooCreateDTO = new ZooCreateDTO(0,  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(zooRepository.findByZooId(id)).willReturn(Optional.of(zoo));
        BDDMockito.given(zooAddressService.findById(any(Integer.class))).willReturn(Optional.of(zooAddress));


        ZooDTO result = zooService.update(id, zooCreateDTO);
        Assertions.assertEquals(zooDTO.getId(), result.getId());
        Assertions.assertEquals(zooDTO.getFeedStorageIds(), result.getFeedStorageIds());
        Assertions.assertEquals(zooDTO.getAnimalTypeIds(), result.getAnimalTypeIds());
        Assertions.assertEquals(zooDTO.getAddress(), result.getAddress());

        Mockito.verify(zooRepository, Mockito.atLeastOnce()).findByZooId(id);
        Mockito.verify(zooAddressService, Mockito.atLeastOnce()).findById(any(Integer.class));
    }
}