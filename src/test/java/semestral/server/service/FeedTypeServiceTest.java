package semestral.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.FeedTypeCreateDTO;
import semestral.server.dto.FeedTypeDTO;
import semestral.server.entity.AnimalType;
import semestral.server.entity.FeedStorage;
import semestral.server.entity.FeedType;
import semestral.server.repository.FeedTypeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class FeedTypeServiceTest {
    @Autowired
    private FeedTypeService feedTypeService;
    @MockBean
    private FeedTypeRepository feedTypeRepository;

    @Test
    void findById() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        FeedType feedType= new FeedType( "Bananas", animalTypes, feedStorages);

        BDDMockito.given(feedTypeRepository.findByFeedTypeId(id)).willReturn(Optional.of(feedType));
        Assertions.assertEquals(feedType, feedTypeService.findById(id).get());

        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findByFeedTypeId(id);
    }

    @Test
    void findByIdAsDTO() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        FeedType feedType= new FeedType( "Beans", animalTypes, feedStorages);

        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, "Beans",
                new ArrayList<Integer>(),
                new ArrayList<Integer>());

        BDDMockito.given(feedTypeRepository.findByFeedTypeId(id)).willReturn(Optional.of(feedType));
        Assertions.assertEquals(feedTypeDTO.getId(), feedTypeService.findByIdAsDTO(id).get().getId());
        Assertions.assertEquals(feedTypeDTO.getTypeName(), feedTypeService.findByIdAsDTO(id).get().getTypeName());
        Assertions.assertEquals(feedTypeDTO.getAnimalTypeIds(), feedTypeService.findByIdAsDTO(id).get().getAnimalTypeIds());
        Assertions.assertEquals(feedTypeDTO.getFeedStorageIds(), feedTypeService.findByIdAsDTO(id).get().getFeedStorageIds());

        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findByFeedTypeId(id);
    }

    @Test
    void findByIds() {
        List<Integer>ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        List<FeedType> feedTypeList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();

        feedTypeList.add(new FeedType( "Bananas", animalTypes, feedStorages));
        feedTypeList.add(new FeedType( "Beans", animalTypes, feedStorages));
        BDDMockito.given(feedTypeRepository.findAllById(ids)).willReturn(feedTypeList);

        Assertions.assertEquals(feedTypeList, feedTypeService.findByIds(ids));
        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findAllById(ids);
    }

    @Test
    void findAll() {
        List<FeedType> feedTypeList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();

        feedTypeList.add(new FeedType( "Bananas", animalTypes, feedStorages));
        feedTypeList.add(new FeedType( "Beans", animalTypes, feedStorages));
        BDDMockito.given(feedTypeRepository.findAll()).willReturn(feedTypeList);

        Assertions.assertEquals(feedTypeList, feedTypeService.findAll());
        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllAsDTO() {
        List<FeedType> feedTypeList = new ArrayList<>();
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();

        feedTypeList.add(new FeedType( "Bananas", animalTypes, feedStorages));
        feedTypeList.add(new FeedType( "Beans", animalTypes, feedStorages));

        BDDMockito.given(feedTypeRepository.findAll()).willReturn(feedTypeList);

        Assertions.assertEquals(feedTypeList.stream().map(FeedType::getFeedTypeId).collect(Collectors.toList()),
                feedTypeService.findAllAsDTO().stream().map(FeedTypeDTO::getId).collect(Collectors.toList()));
        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void create() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        FeedType feedType= new FeedType( "Beans", animalTypes, feedStorages);

        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, "Beans",
                new ArrayList<Integer>(),
                new ArrayList<Integer>());
        FeedTypeCreateDTO feedTypeCreateDTO = new FeedTypeCreateDTO("Beans",  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(feedTypeRepository.save(Mockito.any(FeedType.class))).willReturn(feedType);

        FeedTypeDTO result = feedTypeService.create(feedTypeCreateDTO);
        Assertions.assertEquals(feedTypeDTO.getId(), result.getId());
        Assertions.assertEquals(feedTypeDTO.getFeedStorageIds(), result.getFeedStorageIds());
        Assertions.assertEquals(feedTypeDTO.getAnimalTypeIds(), result.getAnimalTypeIds());
        Assertions.assertEquals(feedTypeDTO.getTypeName(), result.getTypeName());

        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).save(Mockito.any(FeedType.class));
    }

    @Test
    void update() {
        Integer id = 0;
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        FeedType feedType= new FeedType( "Beans", animalTypes, feedStorages);

        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, "Beans",
                new ArrayList<Integer>(),
                new ArrayList<Integer>());
        FeedTypeCreateDTO feedTypeCreateDTO = new FeedTypeCreateDTO("Beans",  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(feedTypeRepository.findByFeedTypeId(id)).willReturn(Optional.of(feedType));

        FeedTypeDTO result = feedTypeService.update(id, feedTypeCreateDTO);
        Assertions.assertEquals(feedTypeDTO.getId(), result.getId());
        Assertions.assertEquals(feedTypeDTO.getFeedStorageIds(), result.getFeedStorageIds());
        Assertions.assertEquals(feedTypeDTO.getAnimalTypeIds(), result.getAnimalTypeIds());
        Assertions.assertEquals(feedTypeDTO.getTypeName(), result.getTypeName());

        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findByFeedTypeId(id);
    }

    @Test
    void findByTypeNameAsDTO() {
        Integer id = 0;
        String typeName = "Bananas";
        List<AnimalType> animalTypes = new ArrayList<>();
        List<FeedStorage> feedStorages = new ArrayList<>();
        FeedType feedType= new FeedType( typeName, animalTypes, feedStorages);

        FeedTypeDTO feedTypeDTO = new FeedTypeDTO(id, typeName, new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(feedTypeRepository.findByTypeName(typeName)).willReturn(Optional.of(feedType));

        Assertions.assertEquals(feedTypeDTO.getId(), feedTypeService.findByTypeNameAsDTO(typeName).get().getId());
        Assertions.assertEquals(feedTypeDTO.getTypeName(), feedTypeService.findByTypeNameAsDTO(typeName).get().getTypeName());
        Assertions.assertEquals(feedTypeDTO.getAnimalTypeIds(), feedTypeService.findByTypeNameAsDTO(typeName).get().getAnimalTypeIds());
        Assertions.assertEquals(feedTypeDTO.getFeedStorageIds(), feedTypeService.findByTypeNameAsDTO(typeName).get().getFeedStorageIds());

        Mockito.verify(feedTypeRepository, Mockito.atLeastOnce()).findByTypeName(typeName);

    }
}