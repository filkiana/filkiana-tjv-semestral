package semestral.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import semestral.server.dto.AnimalTypeCreateDTO;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.entity.AnimalType;
import semestral.server.entity.FeedType;
import semestral.server.entity.Zoo;
import semestral.server.repository.AnimalTypeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class AnimalTypeServiceTest {
    @Autowired
    private AnimalTypeService animalTypeService;
    @MockBean
    private AnimalTypeRepository animalTypeRepository;

    @Test
    void findByIdAsDTO() {
        Integer id = 0;
        List<FeedType> feedTypes = new ArrayList<>();
        List<Zoo> zoos = new ArrayList<>();
        AnimalType animalType= new AnimalType( "Dog", feedTypes, zoos);

        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(id, "Dog",
                new ArrayList<Integer>(),
               new ArrayList<Integer>());

        BDDMockito.given(animalTypeRepository.findByAnimalTypeId(id)).willReturn(Optional.of(animalType));
        Assertions.assertEquals(animalTypeDTO.getId(), animalTypeService.findByIdAsDTO(id).get().getId());
        Assertions.assertEquals(animalTypeDTO.getTypeName(), animalTypeService.findByIdAsDTO(id).get().getTypeName());
        Assertions.assertEquals(animalTypeDTO.getFeedTypeIds(), animalTypeService.findByIdAsDTO(id).get().getFeedTypeIds());
        Assertions.assertEquals(animalTypeDTO.getZooIds(), animalTypeService.findByIdAsDTO(id).get().getZooIds());

        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findByAnimalTypeId(id);
    }

    @Test
    void findByIds() {
        List<Integer>ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        List<AnimalType> animalTypeList = new ArrayList<>();
        List<FeedType> feedTypes = new ArrayList<>();
        List<Zoo> zoos = new ArrayList<>();

        animalTypeList.add(new AnimalType( "Cat", feedTypes, zoos));
        animalTypeList.add(new AnimalType( "Dog", feedTypes, zoos));
        BDDMockito.given(animalTypeRepository.findAllById(ids)).willReturn(animalTypeList);

        Assertions.assertEquals(animalTypeList, animalTypeService.findByIds(ids));
        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findAllById(ids);
    }

    @Test
    void findAll() {
        List<AnimalType> animalTypeList = new ArrayList<>();
        List<FeedType> feedTypes = new ArrayList<>();
        List<Zoo> zoos = new ArrayList<>();
        animalTypeList.add(new AnimalType( "Cat",
                feedTypes,
                zoos));
        animalTypeList.add(new AnimalType( "Dog",
                feedTypes,
                zoos));
        BDDMockito.given(animalTypeRepository.findAll()).willReturn(animalTypeList);

        Assertions.assertEquals(animalTypeList, animalTypeService.findAll());
        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllAsDTO() {
        List<AnimalType> animalTypeList = new ArrayList<>();
        List<FeedType> feedTypes = new ArrayList<>();
        List<Zoo> zoos = new ArrayList<>();
        animalTypeList.add(new AnimalType( "Cat",
                feedTypes,
                zoos));
        animalTypeList.add(new AnimalType( "Dog",
                feedTypes,
                zoos));
        BDDMockito.given(animalTypeRepository.findAll()).willReturn(animalTypeList);

        Assertions.assertEquals(animalTypeList.stream().map(AnimalType::getAnimalTypeId).collect(Collectors.toList()),
                animalTypeService.findAllAsDTO().stream().map(AnimalTypeDTO::getId).collect(Collectors.toList()));
        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void create() {
        Integer id = 0;
        List<Zoo> zoos = new ArrayList<>();
        List<FeedType> feedTypes = new ArrayList<>();

        AnimalType animalType = new AnimalType( "Dog", feedTypes, zoos);
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(id, "Dog", new ArrayList<Integer>(), new ArrayList<Integer>());
        AnimalTypeCreateDTO animalTypeCreateDTO = new AnimalTypeCreateDTO("Dog",  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(animalTypeRepository.save(Mockito.any(AnimalType.class))).willReturn(animalType);

        AnimalTypeDTO result = animalTypeService.create(animalTypeCreateDTO);
        Assertions.assertEquals(animalTypeDTO.getId(), result.getId());
        Assertions.assertEquals(animalTypeDTO.getZooIds(), result.getZooIds());
        Assertions.assertEquals(animalTypeDTO.getFeedTypeIds(), result.getFeedTypeIds());
        Assertions.assertEquals(animalTypeDTO.getTypeName(), result.getTypeName());

        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).save(Mockito.any(AnimalType.class));
    }

    @Test
    void update() {
        Integer id = 0;
        List<Zoo> zoos = new ArrayList<>();
        List<FeedType> feedTypes = new ArrayList<>();

        AnimalType animalType = new AnimalType( "Dog", feedTypes, zoos);
        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(id, "Dog", new ArrayList<Integer>(), new ArrayList<Integer>());
        AnimalTypeCreateDTO animalTypeCreateDTO = new AnimalTypeCreateDTO("Dog",  new ArrayList<Integer>(), new ArrayList<Integer>());

        BDDMockito.given(animalTypeRepository.findByAnimalTypeId(id)).willReturn(Optional.of(animalType));

        AnimalTypeDTO result = animalTypeService.update(id, animalTypeCreateDTO);
        Assertions.assertEquals(animalTypeDTO.getId(), result.getId());
        Assertions.assertEquals(animalTypeDTO.getZooIds(), result.getZooIds());
        Assertions.assertEquals(animalTypeDTO.getFeedTypeIds(), result.getFeedTypeIds());
        Assertions.assertEquals(animalTypeDTO.getTypeName(), result.getTypeName());

        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findByAnimalTypeId(id);
    }

    @Test
    void findByTypeNameAsDTO() {
        String typeName = "Dog";
        List<FeedType> feedTypes = new ArrayList<>();
        List<Zoo> zoos = new ArrayList<>();
        AnimalType animalType= new AnimalType( typeName, feedTypes, zoos);

        AnimalTypeDTO animalTypeDTO = new AnimalTypeDTO(0, typeName,
                new ArrayList<Integer>(),
                new ArrayList<Integer>());

        BDDMockito.given(animalTypeRepository.findByTypeName(typeName)).willReturn(Optional.of(animalType));

        Assertions.assertEquals(animalTypeDTO.getId(), animalTypeService.findByTypeNameAsDTO(typeName).get().getId());
        Assertions.assertEquals(animalTypeDTO.getTypeName(), animalTypeService.findByTypeNameAsDTO(typeName).get().getTypeName());
        Assertions.assertEquals(animalTypeDTO.getFeedTypeIds(), animalTypeService.findByTypeNameAsDTO(typeName).get().getFeedTypeIds());
        Assertions.assertEquals(animalTypeDTO.getZooIds(), animalTypeService.findByTypeNameAsDTO(typeName).get().getZooIds());

        Mockito.verify(animalTypeRepository, Mockito.atLeastOnce()).findByTypeName(typeName);
    }
}