package semestral.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import semestral.server.entity.FeedType;

import java.util.List;
import java.util.Optional;

public interface FeedTypeRepository extends JpaRepository<FeedType, Integer> {
    Optional<FeedType> findByFeedTypeId(Integer id);

    void deleteByFeedTypeId(int id);

    @Override
    void deleteAll();

    @Override
    List<FeedType> findAll();

    Optional<FeedType> findByTypeName(String typeName);
}

