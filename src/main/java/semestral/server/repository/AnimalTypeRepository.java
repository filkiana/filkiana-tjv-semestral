package semestral.server.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import semestral.server.entity.AnimalType;


import java.util.List;
import java.util.Optional;

public interface AnimalTypeRepository extends JpaRepository<AnimalType, Integer> {
    Optional<AnimalType> findByAnimalTypeId(Integer integer);

    void deleteByAnimalTypeId(int id);

    @Override
    void deleteAll();

    @Override
    List<AnimalType> findAll();

    Optional<AnimalType> findByTypeName(String typeName);
}
