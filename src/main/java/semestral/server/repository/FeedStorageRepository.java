package semestral.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import semestral.server.entity.FeedStorage;

import java.util.List;
import java.util.Optional;

public interface FeedStorageRepository extends JpaRepository<FeedStorage, Integer> {
    Optional<FeedStorage> findByFeedStorageId(Integer integer);

    void deleteByFeedStorageId(int id);

    @Override
    void deleteAll();

    @Override
    List<FeedStorage> findAll();
}
