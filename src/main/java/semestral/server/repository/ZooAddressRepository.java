package semestral.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import semestral.server.entity.ZooAddress;

import java.util.List;
import java.util.Optional;

public interface ZooAddressRepository extends JpaRepository<ZooAddress, Integer> {

    Optional<ZooAddress> findByZooAddressId(Integer id);

    void deleteByZooAddressId(int id);

    @Override
    void deleteAll();


    @Override
    List<ZooAddress> findAll();

    List<ZooAddress> findAddressByCity(String city);

    List<ZooAddress> findAddressByStreet(String street);

    List<ZooAddress> findAddressByHouseNumber(Integer houseNumber);



}
