package semestral.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import semestral.server.entity.Zoo;

import java.util.List;
import java.util.Optional;

public interface ZooRepository extends JpaRepository<Zoo, Integer> {
    void deleteByZooId(int id);

    @Override
    void deleteAll();

    Optional<Zoo> findByZooId(Integer integer);

}
