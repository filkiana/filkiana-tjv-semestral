package semestral.server.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import semestral.server.dto.ZooAddressCreateDTO;
import semestral.server.dto.ZooAddressDTO;
import semestral.server.service.ZooAddressService;

import java.util.List;

@RestController
public class ZooAddressController {
    private final ZooAddressService zooAddressService;

    @Autowired
    public ZooAddressController(ZooAddressService zooAddressService) {
        this.zooAddressService = zooAddressService;
    }

    @GetMapping("/address/all")
    List<ZooAddressDTO> all() {
        return zooAddressService.findAllAsDTO();
    }

    @GetMapping("/address/{id}")
    ZooAddressDTO byId(@PathVariable int id) {
        return zooAddressService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
    @GetMapping(path = "/address", params = {"city"})
    List<ZooAddressDTO> byCity(@RequestParam String city) {
        return zooAddressService.findAllByCityAsDTO(city);
    }

    @GetMapping(path = "/address", params = {"street"})
    List<ZooAddressDTO> byStreet(@RequestParam("street") String street) {
        return zooAddressService.findAllByStreetAsDTO(street);
    }
    @GetMapping(path = "/address", params = {"houseNumber"})
    List<ZooAddressDTO> byHouseNumber(@RequestParam int houseNumber) {
        return zooAddressService.findAllByHouseNumberAsDTO(houseNumber);
    }

    @PostMapping("/address")
    ZooAddressDTO create(@RequestBody ZooAddressCreateDTO address) {
        return zooAddressService.create(address);
    }

    @PutMapping("/address/{id}")
    ZooAddressDTO update(@PathVariable int id, @RequestBody ZooAddressCreateDTO address) throws IllegalArgumentException {
        return zooAddressService.update(id, address);
    }

    @DeleteMapping("/address/{id}")
    void delete(@PathVariable int id) throws Exception {
        zooAddressService.deleteById(id);
    }

    @DeleteMapping("/address/all")
    void deleteAll() throws Exception {
        zooAddressService.deleteAll();
    }
}
