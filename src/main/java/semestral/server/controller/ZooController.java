package semestral.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import semestral.server.dto.ZooCreateDTO;
import semestral.server.dto.ZooDTO;
import semestral.server.service.ZooService;


import java.util.List;

@RestController
public class ZooController {
    private final ZooService zooService;

    @Autowired
    public ZooController(ZooService zooService) {
        this.zooService = zooService;
    }

    @GetMapping("/zoo/all")
    List<ZooDTO> all() {
        return zooService.findAllAsDTO();
    }
    @GetMapping("/zoo/allOpened")
    List<ZooDTO> allOpened() {
        return zooService.findAllOpenedAsDTO();
    }

    @GetMapping("/zoo/{id}")
    ZooDTO byId(@PathVariable int id) {
        return zooService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/zoo")
    ZooDTO create(@RequestBody ZooCreateDTO zoo) throws IllegalArgumentException {
        return zooService.create(zoo);
    }

    @PutMapping("/zoo/{id}")
    ZooDTO update(@PathVariable int id, @RequestBody ZooCreateDTO zoo) throws IllegalArgumentException {
        return zooService.update(id, zoo);
    }
    @DeleteMapping("/zoo/{id}")

    void delete(@PathVariable int id) {
        zooService.deleteByZooId(id);
    }

    @DeleteMapping("/zoo/all")
    void delete() {
        zooService.deleteAll();
    }
    @DeleteMapping("/zoo/allClosed")
    void deleteAllClosed() {
        zooService.deleteAllClosed();
    }
}
