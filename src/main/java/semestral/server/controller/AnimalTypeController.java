package semestral.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.service.AnimalTypeService;
import semestral.server.dto.AnimalTypeCreateDTO;

import java.util.List;

@RestController
public class AnimalTypeController {

    private final AnimalTypeService animalTypeService;

    @Autowired
    public AnimalTypeController(AnimalTypeService animalTypeService) {
        this.animalTypeService = animalTypeService;
    }

    @GetMapping("/animalType/all")
    List<AnimalTypeDTO> all() {
        return animalTypeService.findAllAsDTO();
    }

    @GetMapping("/animalType/{id}")
    AnimalTypeDTO byId(@PathVariable int id) {
        return animalTypeService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(path = "/animalType", params = {"typeName"})
    AnimalTypeDTO byTypeName(@RequestParam String typeName) {
        return animalTypeService.findByTypeNameAsDTO(typeName).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/animalType")
    AnimalTypeDTO create(@RequestBody AnimalTypeCreateDTO animalTypeCreateDTO) throws IllegalArgumentException {
        return animalTypeService.create(animalTypeCreateDTO);
    }

    @PutMapping("/animalType/{id}")
    AnimalTypeDTO update(@PathVariable int id, @RequestBody AnimalTypeCreateDTO animalTypeCreateDTO) throws IllegalArgumentException {
        return animalTypeService.update(id, animalTypeCreateDTO);
    }

    @DeleteMapping("/animalType/{id}")
    void delete(@PathVariable int id) {
        animalTypeService.deleteByAnimalTypeId(id);
    }

    @DeleteMapping("/animalType/all")
    void delete() {
        animalTypeService.deleteAll();
    }
}