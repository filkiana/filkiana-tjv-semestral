package semestral.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import semestral.server.dto.FeedStorageDTO;
import semestral.server.dto.FeedStorageCreateDTO;
import semestral.server.service.FeedStorageService;

import java.util.List;

@RestController
public class FeedStorageController {
    private final FeedStorageService feedStorageService;

    @Autowired
    public FeedStorageController(FeedStorageService feedStorageService) {
        this.feedStorageService = feedStorageService;
    }

    @GetMapping("/feedStorage/all")
    List<FeedStorageDTO> all() {
        return feedStorageService.findAllAsDTO();
    }

    @GetMapping("/feedStorage/{id}")
    FeedStorageDTO byId(@PathVariable int id) {
        return feedStorageService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/feedStorage")
    FeedStorageDTO create(@RequestBody FeedStorageCreateDTO feedStorageCreateDTO) throws IllegalArgumentException {
        return feedStorageService.create(feedStorageCreateDTO);
    }

    @PutMapping("/feedStorage/{id}")
    FeedStorageDTO update(@PathVariable int id, @RequestBody FeedStorageCreateDTO feedStorageCreateDTO) throws IllegalArgumentException {
        return feedStorageService.update(id, feedStorageCreateDTO);
    }

    @DeleteMapping("/feedStorage/{id}")
    void delete(@PathVariable int id) {
        feedStorageService.deleteByFeedStorageId(id);
    }

    @DeleteMapping("/feedStorage/all")
    void delete() {
        feedStorageService.deleteAll();
    }

}
