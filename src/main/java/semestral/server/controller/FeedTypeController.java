package semestral.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import semestral.server.dto.FeedTypeDTO;
import semestral.server.dto.FeedTypeCreateDTO;
import semestral.server.service.FeedTypeService;

import java.util.List;

@RestController
public class FeedTypeController {
    private final FeedTypeService feedTypeService;

    @Autowired
    public FeedTypeController(FeedTypeService feedTypeService) {
        this.feedTypeService = feedTypeService;
    }

    @GetMapping("/feedType/all")
    List<FeedTypeDTO> all() {
        return feedTypeService.findAllAsDTO();
    }

    @GetMapping("/feedType/{id}")
    FeedTypeDTO byId(@PathVariable int id) {
        return feedTypeService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(path = "/feedType", params = {"typeName"})
    FeedTypeDTO byTypeName(@RequestParam String typeName) {
        return feedTypeService.findByTypeNameAsDTO(typeName).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/feedType")
    FeedTypeDTO create(@RequestBody FeedTypeCreateDTO feedTypeCreateDTO) throws IllegalArgumentException {
        return feedTypeService.create(feedTypeCreateDTO);
    }

    @PutMapping("/feedType/{id}")
    FeedTypeDTO update(@PathVariable int id, @RequestBody FeedTypeCreateDTO feedTypeCreateDTO) throws IllegalArgumentException {
        return feedTypeService.update(id, feedTypeCreateDTO);
    }

    @DeleteMapping("/feedType/{id}")
    void delete(@PathVariable int id) {
        feedTypeService.deleteByFeedTypeId(id);
    }

    @DeleteMapping("/feedType/all")
    void delete() {
        feedTypeService.deleteAll();
    }

}
