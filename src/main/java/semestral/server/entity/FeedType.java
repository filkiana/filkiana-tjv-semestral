package semestral.server.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
public class FeedType {
    @Id
    @GeneratedValue
    private int feedTypeId;

    @NotNull
    private String typeName;


    @ManyToMany
    @JoinTable(
            name = "FEED_TYPE_ANIMAL_TYPE",
            joinColumns = @JoinColumn(name = "FEED_TYPE_ID"),
            inverseJoinColumns = @JoinColumn(name = "ANIMAL_TYPE_ID")
    )
    private List<AnimalType> animalTypes;

    @OneToMany(mappedBy = "feedType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<FeedStorage> feedStorages;


    public FeedType(String typeName, List<AnimalType> animalTypes, List<FeedStorage> feedStorages) {
        this.typeName = typeName;
        this.animalTypes = animalTypes;
        this.feedStorages = feedStorages;
    }

    public FeedType() {

    }

    public int getFeedTypeId() {
        return feedTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<AnimalType> getAnimalTypes() {
        return animalTypes;
    }

    public void setAnimalTypes(List<AnimalType> animalTypes) {
        this.animalTypes = animalTypes;
    }

    public List<FeedStorage> getFeedStorages() {
        return feedStorages;
    }

    public void setFeedStorages(List<FeedStorage> feedStorages) {
        this.feedStorages = feedStorages;
    }
}
