package semestral.server.entity;

import com.sun.istack.NotNull;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ZooAddress {
    @Id
    @GeneratedValue
    private int zooAddressId;

    @NotNull
    private String city;

    @NotNull
    private String street;

    @NotNull
    private int houseNumber;

    public ZooAddress(String city, String street, int houseNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public ZooAddress() {

    }

    public int getZooAddressId() {
        return zooAddressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int number) {
        this.houseNumber = number;
    }
}
