package semestral.server.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Zoo {

    @Id
    @GeneratedValue
    private int zooId;

    @OneToOne
    private ZooAddress zooAddress;

    @OneToMany(mappedBy = "zoo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<FeedStorage> feedStorages;

    @ManyToMany
    @JoinTable(
            name = "ZOO_ANIMAL_TYPE",
            joinColumns = @JoinColumn(name = "ZOO_ID"),
            inverseJoinColumns = @JoinColumn(name = "ANIMALTYPE_ID")
    )
    private List<AnimalType> animalTypesInZoo;

    public Zoo(ZooAddress zooAddress, List<FeedStorage> feedStorages, List<AnimalType> animalTypesInZoo) {
        this.zooAddress = zooAddress;
        this.feedStorages = feedStorages;
        this.animalTypesInZoo = animalTypesInZoo;
    }

    public Zoo() {

    }

    public int getZooId() {
        return zooId;
    }

    public ZooAddress getAddress() {
        return zooAddress;
    }

    public void setAddress(ZooAddress zooAddress) {
        this.zooAddress = zooAddress;
    }

    public List<FeedStorage> getFeedStorages() {
        return feedStorages;
    }

    public void setFeedStorages(List<FeedStorage> feedStorages) {
        this.feedStorages = feedStorages;
    }

    public List<AnimalType> getAnimalTypesInZoo() {
        return animalTypesInZoo;
    }

    public void setAnimalTypesInZoo(List<AnimalType> animals) {
        this.animalTypesInZoo = animals;
    }
}
