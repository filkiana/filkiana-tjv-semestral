package semestral.server.entity;

import javax.persistence.*;

@Entity
public class FeedStorage {
    @Id
    @GeneratedValue
    private int feedStorageId;

    @ManyToOne
    @JoinColumn(name = "FEED_TYPE")
    private FeedType feedType;

    private int count;

    @ManyToOne
    @JoinColumn(name = "ZOO")
    private Zoo zoo;

    public FeedStorage(FeedType feedType, int count, Zoo zoo) {
        this.feedType = feedType;
        this.count = count;
        this.zoo = zoo;
    }

    public FeedStorage() {

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getFeedStorageId() {
        return feedStorageId;
    }

    public FeedType getFeedType() {
        return feedType;
    }

    public void setFeedType(FeedType feedType) {
        this.feedType = feedType;
    }

    public Zoo getZoo() {
        return zoo;
    }

    public void setZoo(Zoo zoo) {
        this.zoo = zoo;
    }

}
