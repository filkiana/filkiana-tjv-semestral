package semestral.server.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
public class AnimalType {

    @Id
    @GeneratedValue
    private int animalTypeId;

    @NotNull
    private String typeName;


    @ManyToMany(mappedBy = "animalTypes", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<semestral.server.entity.FeedType> feedTypes;


    @ManyToMany(mappedBy = "animalTypesInZoo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<semestral.server.entity.Zoo> zoos;

    public AnimalType(String typeName, List<semestral.server.entity.FeedType> feedTypes, List<semestral.server.entity.Zoo> zoos) {
        this.typeName = typeName;
        this.feedTypes = feedTypes;
        this.zoos = zoos;
    }

    public List<semestral.server.entity.Zoo> getZoos() {
        return zoos;
    }

    public void setZoos(List<semestral.server.entity.Zoo> zoos) {
        this.zoos = zoos;
    }

    public AnimalType() {

    }

    public int getAnimalTypeId() {
        return animalTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<semestral.server.entity.FeedType> getFeedTypes() {
        return feedTypes;
    }

    public void setFeedTypes(List<semestral.server.entity.FeedType> feedTypes) {
        this.feedTypes = feedTypes;
    }
}
