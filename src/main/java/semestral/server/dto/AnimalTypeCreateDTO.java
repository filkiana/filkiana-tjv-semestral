package semestral.server.dto;

import java.util.List;

public class AnimalTypeCreateDTO {
    private final String typeName;
    private final List<Integer> feedTypeIds;
    private final List<Integer> zooIds;

    public AnimalTypeCreateDTO(String typeName, List<Integer> feedTypeIds, List<Integer> zooIds) {
        this.typeName = typeName;
        this.feedTypeIds = feedTypeIds;
        this.zooIds = zooIds;
    }

    public List<Integer> getZooIds() {
        return zooIds;
    }

    public String getTypeName() {
        return typeName;
    }

    public List<Integer> getFeedTypeIds() {
        return feedTypeIds;
    }
}
