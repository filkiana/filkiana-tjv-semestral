package semestral.server.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FeedStorageDTO {
    private final int id;
    private final Integer feedTypeId;
    private final Integer zooId;
    private final int count;

    public FeedStorageDTO(int id, Integer feedTypeId, Integer zooId, int count) {
        this.id = id;
        this.feedTypeId = feedTypeId;
        this.zooId = zooId;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public Integer getFeedTypeId() {
        return feedTypeId;
    }

    public int getCount() {
        return count;
    }

    public Integer getZooId() {
        return zooId;
    }
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
