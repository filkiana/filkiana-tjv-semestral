package semestral.server.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class ZooDTO {
    private final int id;
    private final Integer zooAddressId;
    private final List<Integer> feedStorageIds;
    private final List<Integer> animalTypeIds;

    public ZooDTO(int id, Integer zooAddressId, List<Integer> feedStorageIds, List<Integer> animalTypeIds) {
        this.id = id;
        this.zooAddressId = zooAddressId;
        this.feedStorageIds = feedStorageIds;
        this.animalTypeIds = animalTypeIds;
    }

    public int getId() {
        return id;
    }

    public Integer getAddress() {
        return zooAddressId;
    }

    public List<Integer> getFeedStorageIds() {
        return feedStorageIds;
    }

    public List<Integer> getAnimalTypeIds() {
        return animalTypeIds;
    }
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
