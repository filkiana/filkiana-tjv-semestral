package semestral.server.dto;

public class FeedStorageCreateDTO {

    private final Integer feedTypeId;
    private final int count;
    private final Integer zooId;

    public FeedStorageCreateDTO(Integer feedTypeId, int count, Integer zooId) {
        this.feedTypeId = feedTypeId;
        this.count = count;
        this.zooId = zooId;
    }

    public Integer getFeedTypeId() {
        return feedTypeId;
    }

    public Integer getZooId() {
        return zooId;
    }

    public int getCount() {
        return count;
    }
}
