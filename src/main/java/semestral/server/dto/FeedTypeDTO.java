package semestral.server.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class FeedTypeDTO {
    private final int id;
    private final String typeName;
    private final List<Integer> animalTypeIds;
    private final List<Integer> feedStorageIds;

    public FeedTypeDTO(int id, String typeName, List<Integer> animalTypeIds, List<Integer> feedStorageIds) {
        this.id = id;
        this.typeName = typeName;
        this.animalTypeIds = animalTypeIds;
        this.feedStorageIds = feedStorageIds;
    }

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public List<Integer> getAnimalTypeIds() {
        return animalTypeIds;
    }

    public List<Integer> getFeedStorageIds() {
        return feedStorageIds;
    }
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
