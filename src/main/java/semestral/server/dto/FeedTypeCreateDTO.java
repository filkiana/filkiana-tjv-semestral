package semestral.server.dto;

import java.util.List;

public class FeedTypeCreateDTO {
    private final String typeName;
    private final List<Integer> animalTypeIds;
    private final List<Integer> feedStorageIds;

    public FeedTypeCreateDTO(String typeName, List<Integer> animalTypeIds, List<Integer> feedStorageIds) {
        this.typeName = typeName;
        this.animalTypeIds = animalTypeIds;
        this.feedStorageIds = feedStorageIds;
    }

    public String getTypeName() {
        return typeName;
    }

    public List<Integer> getAnimalTypeIds() {
        return animalTypeIds;
    }

    public List<Integer> getFeedStorageIds() {
        return feedStorageIds;
    }
}
