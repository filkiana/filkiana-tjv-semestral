package semestral.server.dto;

public class ZooAddressCreateDTO {

    private final String city;
    private final String street;
    private final int houseNumber;

    public ZooAddressCreateDTO(String city, String street, int houseNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

}
