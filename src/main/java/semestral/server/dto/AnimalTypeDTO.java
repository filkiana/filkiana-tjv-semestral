package semestral.server.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class AnimalTypeDTO {
    private final int id;
    private final String typeName;
    private final List<Integer> feedTypeIds;
    private final List<Integer> zooIds;

    public AnimalTypeDTO(int id, String typeName, List<Integer> feedTypeIds, List<Integer> zooIds) {
        this.id = id;
        this.typeName = typeName;
        this.feedTypeIds = feedTypeIds;
        this.zooIds = zooIds;
    }

    public List<Integer> getZooIds() {
        return zooIds;
    }

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public List<Integer> getFeedTypeIds() {
        return feedTypeIds;
    }
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

}
