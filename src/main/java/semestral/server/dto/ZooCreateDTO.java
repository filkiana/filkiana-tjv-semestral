package semestral.server.dto;

import java.util.List;

public class ZooCreateDTO {
    private final Integer zooAddressId;
    private final List<Integer> feedStorageIds;
    private final List<Integer> animalTypeIds;

    public ZooCreateDTO(Integer zooAddressId, List<Integer> feedStorageIds, List<Integer> animalTypeIds) {
        this.zooAddressId = zooAddressId;
        this.feedStorageIds = feedStorageIds;
        this.animalTypeIds = animalTypeIds;
    }

    public Integer getAddress() {
        return zooAddressId;
    }

    public List<Integer> getFeedStorageIds() {
        return feedStorageIds;
    }

    public List<Integer> getAnimalTypeIds() {
        return animalTypeIds;
    }

}
