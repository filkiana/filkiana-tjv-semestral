package semestral.server.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ZooAddressDTO {
    private final int id;
    private final String city;
    private final String street;
    private final int houseNumber;

    public ZooAddressDTO(int id, String city, String street, int houseNumber) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
