package semestral.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import semestral.server.dto.AnimalTypeDTO;
import semestral.server.dto.AnimalTypeCreateDTO;
import semestral.server.entity.AnimalType;
import semestral.server.entity.FeedType;
import semestral.server.entity.Zoo;
import semestral.server.repository.AnimalTypeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnimalTypeService {
    private final AnimalTypeRepository animalTypeRepository;
    private final FeedTypeService feedTypeService;
    private final ZooService zooService;

    @Autowired
    public AnimalTypeService(AnimalTypeRepository animalTypeRepository, FeedTypeService feedTypeService, ZooService zooService) {
        this.animalTypeRepository = animalTypeRepository;
        this.feedTypeService = feedTypeService;
        this.zooService = zooService;
    }

    public Optional<AnimalTypeDTO> findByIdAsDTO(int id) {
        return toDTO(animalTypeRepository.findByAnimalTypeId(id));
    }

    public List<AnimalType> findByIds(List<Integer> ids) {
        return animalTypeRepository.findAllById(ids);
    }

    public List<AnimalType> findAll() {
        return animalTypeRepository.findAll();
    }

    public List<AnimalTypeDTO> findAllAsDTO() {
        return animalTypeRepository
                .findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public AnimalTypeDTO create(AnimalTypeCreateDTO animalTypeCreateDTO) throws IllegalArgumentException {
        List<FeedType> feedTypes = null;
        List<Zoo> zoos = null;
        if (animalTypeCreateDTO.getFeedTypeIds() != null) {
            feedTypes = feedTypeService.findByIds(animalTypeCreateDTO.getFeedTypeIds());
            if (feedTypes.size() != animalTypeCreateDTO.getFeedTypeIds().size()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (animalTypeCreateDTO.getZooIds() != null) {
            zoos = zooService.findByIds(animalTypeCreateDTO.getFeedTypeIds());
            if (zoos.size() != animalTypeCreateDTO.getZooIds().size()) {
                throw new IllegalArgumentException("Some types of zoo not found");
            }
        }
        return toDTO(animalTypeRepository.save(new AnimalType(animalTypeCreateDTO.getTypeName(), feedTypes, zoos)));
    }

    @Transactional
    public AnimalTypeDTO update(int id, AnimalTypeCreateDTO animalTypeCreateDTO) throws IllegalArgumentException {
        Optional<AnimalType> optionalAnimalType = animalTypeRepository.findByAnimalTypeId(id);
        if (optionalAnimalType.isEmpty()) {
            throw new IllegalArgumentException("No such AnimalType");
        }
        List<FeedType> feedTypes = null;
        List<Zoo> zoos = null;
        if (animalTypeCreateDTO.getFeedTypeIds() != null) {
            feedTypes = feedTypeService.findByIds(animalTypeCreateDTO.getFeedTypeIds());
            if (feedTypes.size() != animalTypeCreateDTO.getFeedTypeIds().size()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (animalTypeCreateDTO.getFeedTypeIds() != null) {
            zoos = zooService.findByIds(animalTypeCreateDTO.getFeedTypeIds());
            if (zoos.size() != animalTypeCreateDTO.getZooIds().size()) {
                throw new IllegalArgumentException("Some types of zoo` not found");
            }
        }
        AnimalType animalType = optionalAnimalType.get();
        animalType.setFeedTypes(feedTypes);
        animalType.setTypeName(animalTypeCreateDTO.getTypeName());
        animalType.setZoos(zoos);
        return toDTO(animalType);
    }

    private AnimalTypeDTO toDTO(AnimalType animalType) {
        List<Integer> feedTypeIds = null;
        List<Integer> zooIds = null;
        if (animalType.getFeedTypes() != null)
            feedTypeIds = animalType.getFeedTypes()
                    .stream()
                    .map(FeedType::getFeedTypeId)
                    .collect(Collectors.toList());

        if (animalType.getZoos() != null)
            zooIds = animalType.getZoos().stream()
                    .map(Zoo::getZooId)
                    .collect(Collectors.toList());
        return new AnimalTypeDTO(animalType.getAnimalTypeId(),
                animalType.getTypeName(),
                feedTypeIds,
                zooIds);
    }

    private Optional<AnimalTypeDTO> toDTO(Optional<AnimalType> optionalAnimalType) {
        if (optionalAnimalType.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(optionalAnimalType.get()));
    }

    @Transactional
    public void deleteByAnimalTypeId(int id) {
        animalTypeRepository.deleteByAnimalTypeId(id);
    }

    @Transactional
    public void deleteAll() {
        animalTypeRepository.deleteAll();
    }

    public Optional<AnimalTypeDTO> findByTypeNameAsDTO(String typeName) {
        return toDTO(animalTypeRepository.findByTypeName(typeName));
    }
}
