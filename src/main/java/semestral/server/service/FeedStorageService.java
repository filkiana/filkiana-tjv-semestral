package semestral.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import semestral.server.dto.FeedStorageDTO;
import semestral.server.entity.Zoo;
import semestral.server.repository.FeedStorageRepository;
import semestral.server.dto.FeedStorageCreateDTO;
import semestral.server.entity.FeedStorage;
import semestral.server.entity.FeedType;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FeedStorageService {
    private final FeedStorageRepository feedStorageRepository;
    private final FeedTypeService feedTypeService;
    private final ZooService zooService;

    @Autowired
    public FeedStorageService(FeedStorageRepository feedStorageRepository, FeedTypeService feedTypeService, ZooService zooService) {
        this.feedStorageRepository = feedStorageRepository;
        this.feedTypeService = feedTypeService;
        this.zooService = zooService;
    }

    public Optional<FeedStorageDTO> findByIdAsDTO(int id) {
        return toDTO(feedStorageRepository.findByFeedStorageId(id));
    }

    public List<FeedStorage> findByIds(List<Integer> ids) {
        return feedStorageRepository.findAllById(ids);
    }

    public List<FeedStorage> findAll() {
        return feedStorageRepository.findAll();
    }

    public List<FeedStorageDTO> findAllAsDTO() {
        return feedStorageRepository
                .findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public FeedStorageDTO create(FeedStorageCreateDTO feedStorageCreateDTO) throws IllegalArgumentException {
        Optional<FeedType> feedType = Optional.empty();
        Optional<Zoo> zoo = Optional.empty();
        if (feedStorageCreateDTO.getFeedTypeId() != null) {
            feedType = feedTypeService.findById(feedStorageCreateDTO.getFeedTypeId());
            if (feedType.isEmpty()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (feedStorageCreateDTO.getZooId() != null) {
            zoo = zooService.findById(feedStorageCreateDTO.getZooId());
            if (zoo.isEmpty()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        return toDTO(feedStorageRepository.save(new FeedStorage(feedType.get(), feedStorageCreateDTO.getCount(), zoo.get())));
    }

    @Transactional
    public FeedStorageDTO update(int id, FeedStorageCreateDTO feedStorageCreateDTO) throws IllegalArgumentException {
        Optional<FeedStorage> optionalFeedStorage = feedStorageRepository.findByFeedStorageId(id);
        if (optionalFeedStorage.isEmpty()) {
            throw new IllegalArgumentException("no such FeedStorage");
        }

        Optional<FeedType> feedType = Optional.empty();
        Optional<Zoo> zoo = Optional.empty();
        if (feedStorageCreateDTO.getFeedTypeId() != null) {
            feedType = feedTypeService.findById(feedStorageCreateDTO.getFeedTypeId());
            if (feedType.isEmpty()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (feedStorageCreateDTO.getZooId() != null) {
            zoo = zooService.findById(feedStorageCreateDTO.getZooId());
            if (zoo.isEmpty()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }

        FeedStorage feedStorage = optionalFeedStorage.get();
        feedStorage.setCount(feedStorageCreateDTO.getCount());
        feedStorage.setZoo(zoo.get());
        feedStorage.setFeedType(feedType.get());

        return toDTO(feedStorage);
    }

    private FeedStorageDTO toDTO(FeedStorage feedStorage) {
        Integer feedTypeId = null;
        Integer zooId = null;
        if (feedStorage.getFeedType() != null)
            feedTypeId = feedStorage.getFeedType().getFeedTypeId();
        if (feedStorage.getZoo() != null)
            zooId = feedStorage.getZoo().getZooId();
        return new FeedStorageDTO(feedStorage.getFeedStorageId(), feedTypeId, zooId, feedStorage.getCount());
    }

    private Optional<FeedStorageDTO> toDTO(Optional<FeedStorage> optionalFeedStorage) {
        if (optionalFeedStorage.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(optionalFeedStorage.get()));
    }

    @Transactional
    public void deleteByFeedStorageId(int id) {
        feedStorageRepository.deleteByFeedStorageId(id);
    }

    @Transactional
    public void deleteAll() {
        feedStorageRepository.deleteAll();
    }
}
