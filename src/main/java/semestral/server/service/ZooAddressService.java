package semestral.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import semestral.server.dto.ZooAddressCreateDTO;
import semestral.server.dto.ZooAddressDTO;
import semestral.server.entity.ZooAddress;
import semestral.server.repository.ZooAddressRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ZooAddressService {
    private final ZooAddressRepository zooAddressRepository;

    @Autowired
    public ZooAddressService(ZooAddressRepository zooAddressRepository) {
        this.zooAddressRepository = zooAddressRepository;
    }

    public Optional<ZooAddress> findById(int id) {
        return zooAddressRepository.findByZooAddressId(id);
    }

    public Optional<ZooAddressDTO> findByIdAsDTO(int id) {
        return toDTO(zooAddressRepository.findByZooAddressId(id));
    }

    public List<ZooAddress> findAll() {
        return zooAddressRepository.findAll();
    }

    public List<ZooAddressDTO> findAllByCityAsDTO(String city) {
        return zooAddressRepository.findAddressByCity(city).stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public List<ZooAddressDTO> findAllByStreetAsDTO(String street) {
        return zooAddressRepository.findAddressByStreet(street).stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public List<ZooAddressDTO> findAllByHouseNumberAsDTO(int houseNumber) {
        return zooAddressRepository.findAddressByHouseNumber(houseNumber).stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public List<ZooAddressDTO> findAllAsDTO() {
        return zooAddressRepository
                .findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public ZooAddressDTO create(ZooAddressCreateDTO zooAddressCreateDTO) {
        return toDTO(
                zooAddressRepository.save(
                        new ZooAddress(zooAddressCreateDTO.getCity(), zooAddressCreateDTO.getStreet(), zooAddressCreateDTO.getHouseNumber())
                )
        );
    }

    @Transactional
    public ZooAddressDTO update(int id, ZooAddressCreateDTO zooAddressCreateDTO) throws IllegalArgumentException {
        Optional<ZooAddress> optionalAddress = zooAddressRepository.findByZooAddressId(id);
        if (optionalAddress.isEmpty()) {
            throw new IllegalArgumentException("No such ZooAddress");
        }
        ZooAddress zooAddress = optionalAddress.get();
        zooAddress.setCity(zooAddressCreateDTO.getCity());
        zooAddress.setStreet(zooAddressCreateDTO.getStreet());
        zooAddress.setHouseNumber(zooAddressCreateDTO.getHouseNumber());
        return toDTO(zooAddress);
    }

    private ZooAddressDTO toDTO(ZooAddress zooAddress) {
        return new ZooAddressDTO(zooAddress.getZooAddressId(), zooAddress.getCity(), zooAddress.getStreet(), zooAddress.getHouseNumber());
    }

    private Optional<ZooAddressDTO> toDTO(Optional<ZooAddress> optionalAddress) {
        if (optionalAddress.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(optionalAddress.get()));
    }

    @Transactional
    public void deleteById(int id) {
        zooAddressRepository.deleteByZooAddressId(id);
    }

    @Transactional
    public void deleteAll() {
        zooAddressRepository.deleteAll();
    }
}
