package semestral.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import semestral.server.dto.ZooCreateDTO;
import semestral.server.dto.ZooDTO;
import semestral.server.entity.AnimalType;
import semestral.server.entity.FeedStorage;
import semestral.server.entity.Zoo;
import semestral.server.entity.ZooAddress;
import semestral.server.repository.ZooRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ZooService {
    private final ZooRepository zooRepository;
    private final AnimalTypeService animalTypeService;
    private final FeedStorageService feedStorageService;
    private final ZooAddressService zooAddressService;

    @Autowired
    public ZooService(ZooRepository zooRepository, @Lazy AnimalTypeService animalTypeService, @Lazy FeedStorageService feedStorageService, ZooAddressService zooAddressService) {
        this.zooRepository = zooRepository;
        this.animalTypeService = animalTypeService;
        this.feedStorageService = feedStorageService;
        this.zooAddressService = zooAddressService;
    }

    public Optional<Zoo> findById(int id) {
        return zooRepository.findByZooId(id);
    }

    public List<ZooDTO> findAllOpenedAsDTO() {
        List<Zoo> zoos =  zooRepository.findAll();
        zoos.removeIf(zoo -> zoo.getAddress() == null && zoo.getAnimalTypesInZoo().isEmpty() && zoo.getFeedStorages().isEmpty());
        return zoos.stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }
    public Optional<ZooDTO> findByIdAsDTO(int id) {
        return toDTO(zooRepository.findByZooId(id));
    }

    public List<Zoo> findByIds(List<Integer> ids) {
        return zooRepository.findAllById(ids);
    }

    public List<Zoo> findAll() {
        return zooRepository.findAll();
    }

    public List<ZooDTO> findAllAsDTO() {
        return zooRepository
                .findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public ZooDTO create(ZooCreateDTO zooCreateDTO) throws IllegalArgumentException {
        Optional<ZooAddress> zooAddress = Optional.empty();
        List<FeedStorage> feedStorages = null;
        List<AnimalType> animalTypes = null;
        if (zooCreateDTO.getFeedStorageIds() != null) {
            feedStorages = feedStorageService.findByIds(zooCreateDTO.getFeedStorageIds());
            if (feedStorages.size() != zooCreateDTO.getFeedStorageIds().size()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (zooCreateDTO.getAddress() != null) {
            zooAddress = zooAddressService.findById(zooCreateDTO.getAddress());
            if (zooAddress.isEmpty()) {
                throw new IllegalArgumentException("no such Address");
            }
        }
        if (zooCreateDTO.getAnimalTypeIds() != null) {
            animalTypes = animalTypeService.findByIds(zooCreateDTO.getAnimalTypeIds());
            if (animalTypes.size() != zooCreateDTO.getAnimalTypeIds().size()) {
                throw new IllegalArgumentException("Some types of animals not found");
            }
        }
        return toDTO(
                zooRepository.save(
                        new Zoo(zooAddress.get(), feedStorages, animalTypes)
                )
        );
    }

    @Transactional
    public ZooDTO update(int id, ZooCreateDTO zooCreateDTO) throws IllegalArgumentException {
        Optional<Zoo> optionalZoo = zooRepository.findByZooId(id);
        Optional<ZooAddress> zooAddress = zooAddressService.findById(zooCreateDTO.getAddress());
        if (optionalZoo.isEmpty()) {
            throw new IllegalArgumentException("no such Zoo");
        }
        if (zooAddress.isEmpty()) {
            throw new IllegalArgumentException("no such Address");
        }
        List<FeedStorage> feedStorages = feedStorageService.findByIds(zooCreateDTO.getFeedStorageIds());
        if (feedStorages.size() != zooCreateDTO.getFeedStorageIds().size()) {
            throw new IllegalArgumentException("Some types of feed not found");
        }
        List<AnimalType> animalTypes = animalTypeService.findByIds(zooCreateDTO.getAnimalTypeIds());
        if (animalTypes.size() != zooCreateDTO.getAnimalTypeIds().size()) {
            throw new IllegalArgumentException("Some types of animals not found");
        }
        Zoo zoo = optionalZoo.get();
        zoo.setAddress(zooAddress.get());
        zoo.setAnimalTypesInZoo(animalTypes);
        zoo.setFeedStorages(feedStorages);
        return toDTO(zoo);
    }

    private ZooDTO toDTO(Zoo zoo) {
        List<Integer> animalTypeIds = null;
        List<Integer> feedStorageIds = null;
        Integer zooAddressId = null;
        if (zoo.getAnimalTypesInZoo() != null)
            animalTypeIds = zoo.getAnimalTypesInZoo()
                    .stream()
                    .map(AnimalType::getAnimalTypeId)
                    .collect(Collectors.toList());
        if (zoo.getFeedStorages() != null)
            feedStorageIds = zoo.getFeedStorages()
                    .stream()
                    .map(FeedStorage::getFeedStorageId)
                    .collect(Collectors.toList());
        if (zoo.getAddress() != null)
            zooAddressId = zoo.getAddress().getZooAddressId();
        return new ZooDTO(zoo.getZooId(), zooAddressId, feedStorageIds, animalTypeIds);
    }

    private Optional<ZooDTO> toDTO(Optional<Zoo> optionalZoo) {
        if (optionalZoo.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(optionalZoo.get()));
    }

    @Transactional
    public void deleteByZooId(int id) {
        zooRepository.deleteByZooId(id);
    }

    @Transactional
    public void deleteAll() {
        zooRepository.deleteAll();
    }

    public void deleteAllClosed() {
        List<Zoo> zoos =  zooRepository.findAll();
        zoos.removeIf(zoo -> zoo.getAddress() != null && !zoo.getAnimalTypesInZoo().isEmpty() && !zoo.getFeedStorages().isEmpty());
        for (Zoo zoo:zoos
             ) {
            deleteByZooId(zoo.getZooId());
        }
    }
}
