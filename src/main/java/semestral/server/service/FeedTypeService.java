package semestral.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import semestral.server.dto.FeedTypeCreateDTO;
import semestral.server.dto.FeedTypeDTO;
import semestral.server.entity.AnimalType;
import semestral.server.entity.FeedStorage;
import semestral.server.entity.FeedType;
import semestral.server.repository.FeedTypeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FeedTypeService {
    private final FeedTypeRepository feedTypeRepository;
    private final AnimalTypeService animalTypeService;
    private final FeedStorageService feedStorageService;

    @Autowired
    public FeedTypeService(FeedTypeRepository feedTypeRepository, @Lazy AnimalTypeService animalTypeService, @Lazy FeedStorageService feedStorageService) {
        this.feedTypeRepository = feedTypeRepository;
        this.animalTypeService = animalTypeService;
        this.feedStorageService = feedStorageService;
    }

    public Optional<FeedType> findById(int id) {
        return feedTypeRepository.findByFeedTypeId(id);
    }

    public Optional<FeedTypeDTO> findByIdAsDTO(int id) {
        return toDTO(feedTypeRepository.findByFeedTypeId(id));
    }

    public List<FeedType> findByIds(List<Integer> ids) {
        return feedTypeRepository.findAllById(ids);
    }

    public List<FeedType> findAll() {
        return feedTypeRepository.findAll();
    }

    public List<FeedTypeDTO> findAllAsDTO() {
        return feedTypeRepository
                .findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors
                        .toList());
    }

    public FeedTypeDTO create(FeedTypeCreateDTO feedTypeCreateDTO) throws IllegalArgumentException {
        List<AnimalType> animalTypes = null;
        List<FeedStorage> feedStorages = null;
        if (feedTypeCreateDTO.getAnimalTypeIds() != null) {
            animalTypes = animalTypeService.findByIds(feedTypeCreateDTO.getAnimalTypeIds());
            if (animalTypes.size() != feedTypeCreateDTO.getAnimalTypeIds().size()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (feedTypeCreateDTO.getFeedStorageIds() != null) {
            feedStorages = feedStorageService.findByIds(feedTypeCreateDTO.getFeedStorageIds());
            if (feedStorages.size() != feedTypeCreateDTO.getFeedStorageIds().size()) {
                throw new IllegalArgumentException("Some feed storages not found");
            }
        }
        return toDTO(
                feedTypeRepository.save(
                        new FeedType(feedTypeCreateDTO.getTypeName(), animalTypes, feedStorages)
                )
        );
    }

    @Transactional
    public FeedTypeDTO update(int id, FeedTypeCreateDTO feedTypeCreateDTO) throws IllegalArgumentException {
        Optional<FeedType> optionalFeedType = feedTypeRepository.findByFeedTypeId(id);
        if (optionalFeedType.isEmpty()) {
            throw new IllegalArgumentException("no such FeedType");
        }
        List<AnimalType> animalTypes = null;
        List<FeedStorage> feedStorages = null;
        if (feedTypeCreateDTO.getAnimalTypeIds() != null) {
            animalTypes = animalTypeService.findByIds(feedTypeCreateDTO.getAnimalTypeIds());
            if (animalTypes.size() != feedTypeCreateDTO.getAnimalTypeIds().size()) {
                throw new IllegalArgumentException("Some types of feed not found");
            }
        }
        if (feedTypeCreateDTO.getFeedStorageIds() != null) {
            feedStorages = feedStorageService.findByIds(feedTypeCreateDTO.getFeedStorageIds());
            if (feedStorages.size() != feedTypeCreateDTO.getFeedStorageIds().size()) {
                throw new IllegalArgumentException("Some feed storages not found");
            }
        }
        FeedType feedType = optionalFeedType.get();
        feedType.setFeedStorages(feedStorages);
        feedType.setAnimalTypes(animalTypes);
        feedType.setTypeName(feedTypeCreateDTO.getTypeName());
        return toDTO(feedType);
    }

    private FeedTypeDTO toDTO(FeedType feedType) {
        List<Integer> animalTypeIds = null;
        List<Integer> feedStorageIds = null;
        if (feedType.getAnimalTypes() != null)
            animalTypeIds = feedType.getAnimalTypes()
                    .stream()
                    .map(AnimalType::getAnimalTypeId)
                    .collect(Collectors.toList());
        if (feedType.getFeedStorages() != null)
            feedStorageIds = feedType.getFeedStorages().stream()
                    .map(FeedStorage::getFeedStorageId)
                    .collect(Collectors.toList());
        return new FeedTypeDTO(feedType.getFeedTypeId(), feedType.getTypeName(), animalTypeIds, feedStorageIds);
    }

    private Optional<FeedTypeDTO> toDTO(Optional<FeedType> optionalFeedType) {
        if (optionalFeedType.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toDTO(optionalFeedType.get()));
    }

    public Optional<FeedTypeDTO> findByTypeNameAsDTO(String typeName) {
        return toDTO(feedTypeRepository.findByTypeName(typeName));
    }

    @Transactional
    public void deleteByFeedTypeId(int id) {
        feedTypeRepository.deleteByFeedTypeId(id);
    }

    @Transactional
    public void deleteAll() {
        feedTypeRepository.deleteAll();
    }
}
